organization:= "kz.darlab"

name := "DOMAIN--onlineshop"

version := "1.1.1"

scalaVersion := "2.11.7"

isSnapshot := true

scalacOptions := Seq("-feature", "-unchecked", "-deprecation", "-encoding", "utf8")

val akkaVersion = "2.5.13"
val json4sVersion = "3.5.3"

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % akkaVersion % "provided",
  "joda-time" % "joda-time" % "2.8.2" % "provided",
  "org.joda" % "joda-convert" % "1.7" % "provided",
  "org.json4s" %% "json4s-native" % json4sVersion % "provided"
)

enablePlugins(JavaAppPackaging)

Revolver.settings