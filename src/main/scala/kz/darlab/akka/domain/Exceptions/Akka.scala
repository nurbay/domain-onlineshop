package kz.darlab.akka.domain.Exceptions

object DarErrorSystem extends ErrorSystem {
  override val system = "DAR"
}

object DarErrorCodes {

  case class INTERNAL_SERVER_ERROR      (override val series: ErrorSeries,
                                         override val system: ErrorSystem)          extends ErrorCode {
    override val code = 1
  }
  case class NOT_FOUND                  (override val series: ErrorSeries,
                                         override val system: ErrorSystem)          extends ErrorCode {
    override val code = 404
  }
  case class BAD_REQUEST                (override val series: ErrorSeries,
                                         override val system: ErrorSystem)          extends ErrorCode {
    override val code = 400
  }
  case class PRODUCT_ALLREADY_EXISTS    (override val series: ErrorSeries,
                                         override val system: ErrorSystem)           extends ErrorCode {
    override val code = 455
  }
  case class PRODUCT_NOT_FOUND          (override val series: ErrorSeries,
                                         override val system: ErrorSystem)           extends ErrorCode {
    override val code = 456
  }
  case class USER_NOT_FOUND             (override val series: ErrorSeries,
                                         override val system: ErrorSystem)           extends ErrorCode {
    override val code = 457
  }
  case class USER_ALLREADY_EXISTS       (override val series: ErrorSeries,
                                         override val system: ErrorSystem)           extends ErrorCode {
    override val code = 458
  }
}

object DarErrorSeries {

  case object CORE extends ErrorSeries {
    override val series = 0
  }
  case object API extends ErrorSeries {
    override val series = 1
  }

}
