package kz.darlab.akka.domain.serializers

import kz.darlab.akka.domain.entities.DAO_._
import kz.darlab.akka.domain.entities.DTO_.{DtoProduct, DtoWebUser, Dto_Aggregate}


trait OnlineShopSerializer {

  val onlineShopTypeHints = List(
    classOf[Customer],
    classOf[Account],
    classOf[Payment],
    classOf[Category],
    classOf[Product],
    classOf[Order],
    classOf[OrderDetail],
    classOf[WebUser],
    classOf[DoLogin],
    classOf[GetProductById],
    classOf[GetProducts],
    classOf[ProductList],
    classOf[DtoProduct],
    classOf[DtoWebUser],
    classOf[Sales],
    classOf[Dto_Aggregate]


  )

}
