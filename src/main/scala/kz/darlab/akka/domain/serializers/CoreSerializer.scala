package kz.darlab.akka.domain.serializers

import kz.darlab.akka.domain.Exceptions.ErrorInfo
import kz.darlab.akka.domain.amqp._
import kz.darlab.akka.domain.amqp.DomainMessage




trait CoreSerializer {

  val coreTypeHints = List(
    classOf[DomainMessage[_]],
    classOf[Request[_]],
    classOf[Response[_]],
    classOf[Accepted],
    classOf[SeqEntity[_]],
    classOf[Event[_]],
    classOf[Error],
    classOf[ErrorInfo]
  )

}
