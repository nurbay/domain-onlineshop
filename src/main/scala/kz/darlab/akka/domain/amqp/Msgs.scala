package kz.darlab.akka.domain.amqp

import kz.darlab.akka.domain.Exceptions.ErrorInfo
import kz.darlab.akka.domain.entities.DomainEntity

object Msgs {

  case object GetProduct      extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "getproducts"
    override val flow         = Some("GET")
    override val routingKey   = key
  }
    case object AddProduct    extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "addproduct"
    override val flow         = Some("POST")
    override val routingKey   = key
  }
  case object DeleteProduct   extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "deleteProduct"
    override val flow         = Some("POST")
    override val routingKey   = key
  }
  case object UserLogin       extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "doLogin"
    override val flow         = Some("GET")
    override val routingKey   = key
  }
  case object UserRegistr     extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "doRegistr"
    override val flow         = Some("POST")
    override val routingKey   = key
  }
  case object UpdateProduct   extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "updateProduct"
    override val flow         = Some("POST")
    override val routingKey   = key
  }
  case object Aggr            extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "AgrFunc"
    override val flow         = Some("POST")
    override val routingKey   = key
  }
  case object Aggr2            extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "AgrFunc2"
    override val flow         = Some("POST")
    override val routingKey   = key
  }
  case object PostTest        extends RequestMessageType {
    override val target       = Some("read")
    override val name         = "PostTest"
    override val flow         = Some("POST")
    override val routingKey   = key
  }

}

trait MessageType {
  val purpose: String // request / event/ command/ error
  val source: Option[String]
  val target: Option[String]
  val flow: Option[String] = None
  val name:String
  val routingKey: String

  def key: String = {
    val route = purpose match  {
      case "request" => s"${target.get}${flow.getOrElse("").toUpperCase}"
      //case "command" => target.get
      case "event" => s"${source.get}${flow.getOrElse("").toUpperCase}"
      case "error" => s"${source.get}${flow.getOrElse("").toUpperCase}"
      case  _ => "none"
    }


    s"$purpose.$route.$name"
  }

  def binding: String = {
    val route = purpose match  {
      case "request" => s"${target.get.toLowerCase}${flow.getOrElse("").toUpperCase}"
      //case "command" => target.get
      case "event" => s"${source.get.toLowerCase}${flow.getOrElse("").toUpperCase}"
      case "error" => s"${source.get.toLowerCase}${flow.getOrElse("").toUpperCase}"
      case  _ => "none"
    }


    s"$purpose.$route.*"
  }
}


trait RequestMessageType extends MessageType {
  override val  purpose = "request"
  override val  source = None

}

trait EventMessageType extends MessageType {
  override val  purpose = "event"
  override val  target = None
}

case class ErrorMessageType(source:Option[String],name: String) extends MessageType {
  override val  purpose = "error"
  override val  target = None
  override val routingKey = key
}


/**
  * Singletone to access message builders
  */
object DomainMessage {

  def error (errorInfo: ErrorInfo,replyTo: Option[String],headers: Map[String, Any]): Error = {
    val messageType = ErrorMessageType(errorInfo.system,errorInfo.code.getOrElse("undefined").toLowerCase)
    Error(headers,errorInfo,replyTo.getOrElse(messageType.key))
  }


  def request[T](messageType: RequestMessageType,replyTo: Option[String],headers: Map[String, Any], body: T): Request[T] = {
    Request[T](replyTo,headers,body,messageType.key)
  }

  def response[T <: DomainEntity](request: Request[_], body: T): Option[Response[T]] = {
    if (request.replyTo.isDefined) {
      Some(Response[T](request.headers,body,request.replyTo.get))
    }else {
      None
    }
  }

  def event[T <: DomainEntity](messageType: EventMessageType,headers: Map[String, Any], body: T): Event[T] = {
    Event[T](headers,body,messageType.key)
  }


}

/**
  * All domain messages
  */
trait DomainMessage[T] extends DomainObject {

  val headers: Map[String, Any]

  val body: T

  val routingKey: String
}




/**
  * Event messages
  */
trait EventMessage[T] extends DomainMessage[T]

/**
  * Command messages
  *
  * @tparam T - Body type
  */
trait CommandMessage[T] extends DomainMessage[T]


case class Response[T <: DomainEntity](headers: Map[String, Any],
                                       body: T,
                                       routingKey: String) extends DomainMessage[T]


case class Request[T](replyTo: Option[String], headers: Map[String, Any],
                      body: T,
                      routingKey: String) extends CommandMessage[T]




case class Event[T <: DomainEntity](headers: Map[String, Any],
                                    body: T,
                                    routingKey: String) extends EventMessage[T]

case class Error(headers: Map[String, Any],
                 body: ErrorInfo,
                 routingKey: String) extends DomainMessage[ErrorInfo]