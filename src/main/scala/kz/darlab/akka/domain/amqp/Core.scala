package kz.darlab.akka.domain.amqp

import kz.darlab.akka.domain.entities.DomainEntity


trait DomainObject extends Serializable


/**
  * Serializable domain entity
  */
//trait DomainAkka extends DomainObject

/**
  * Default message returned to user when the information is accepted
  *
  * @param status
  * @param message
  */
case class Accepted(status: Int = 200, message: Option[String] = Some("OK")) extends DomainEntity


/**
  * Wrapper case class that contains a sequence of domain entity
  * @param entities - a sequence of domain entity
  */
case class SeqEntity[T](entities: Seq[T]) extends DomainEntity

// Rabbit routing
case class EcoService(system: String, subSystem: String, microService: String) extends DomainEntity {

  def serviceEndpoint = s"$microService.$subSystem.$system"
}

case class Endpoint(instanceId: String, ecoService: EcoService) extends DomainEntity {

  val serviceEndpoint = ecoService.serviceEndpoint

  val instanceEndpoint = s"$instanceId.$serviceEndpoint"

  def queue = s"Q:$instanceEndpoint"

  def exchange = s"X:$instanceEndpoint"
}

trait CustomEnum[T] {

  val values: Seq[T]

  def apply(value: String): T = valueOf(value)

  def valueOf(value: String): T = values.find(_.toString == value).getOrElse(throw new RuntimeException("Value not found"))

}
