package kz.darlab.akka.domain.entities

import kz.darlab.akka.domain.amqp.DomainObject

trait  DomainEntity extends DomainObject
trait ApiAkkaRequest
trait  DomainRabbit extends Serializable

trait DTO extends DomainEntity
trait DAO extends DomainEntity