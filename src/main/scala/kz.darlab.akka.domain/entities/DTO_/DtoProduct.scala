package kz.darlab.akka.domain.entities.DTO_

import kz.darlab.akka.domain.entities.DTO

case class DtoProduct (
                      category_id: Int,
                      name: String,
                      supplier: Option[String] = None,
                      description: Option[String]
                    ) extends DTO



