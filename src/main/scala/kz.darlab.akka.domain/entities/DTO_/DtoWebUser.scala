package kz.darlab.akka.domain.entities.DTO_

import kz.darlab.akka.domain.entities.DTO

case class DtoWebUser (
                      login: String,
                      password: String
                    ) extends DTO

