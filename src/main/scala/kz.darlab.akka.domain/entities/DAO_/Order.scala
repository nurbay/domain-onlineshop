package kz.darlab.akka.domain.entities.DAO_



import kz.darlab.akka.domain.entities.DAO
import org.joda.time.DateTime



case class Order(
                  id: Int,
                  ordered: Option[DateTime],
                  delivered: Option[DateTime],
                  deliver_to: String,
                  status: OrderStatus,
                  total: Double,
                  orderDetail :OrderDetail
                 )  extends  DAO
case class OrderDetail(
                        product_id: Map[Int,Int],
                        total_price: Double
                      ) extends  DAO


case class Payment(
                    id: Int,
                    account_id: Int,
                    paid: DateTime,
                    total: Double,
                    payment_details: Option[String] = None
                  )  extends  DAO


case class Sales (
                 id :Int,
                 product_name :String,
                 quantity :Int,
                 total_sum:Double,
                 date: DateTime
                 ) extends  DAO

trait OrderStatus {
  var orderStatus = ""
}
final case class Delivered() extends  OrderStatus{
  orderStatus = "Delivered"
}
final case class New() extends  OrderStatus{
  orderStatus = "New"
}
final case class Hold() extends  OrderStatus{
  orderStatus = "Hold"
}
final case class Shipped() extends  OrderStatus{
  orderStatus = "Shipped"
}
final case class Closed() extends  OrderStatus{
  orderStatus = "Closed"
}



