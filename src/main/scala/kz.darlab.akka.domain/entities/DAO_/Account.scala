package kz.darlab.akka.domain.entities.DAO_



import kz.darlab.akka.domain.entities.{DAO}
import org.joda.time.DateTime


case class Account(
                    id: String,
                    customer_id: Int,
                    billing_address: String,
                    is_closed: Boolean,
                    open: DateTime,
                    close: DateTime,
                    email: Option[String],
                    address: Option[String]
                     ) extends DAO

case class Customer(
                     id: String,
                     full_name: String,
                     phone: String,
                     age: Option[Int],
                     email: Option[String],
                     address: String
                   )  extends DAO


