package kz.darlab.akka.domain.entities.DAO_

import kz.darlab.akka.domain.entities.{DAO}


case class WebUser(
                    id: Int,
                    login: String,
                    password: String,
                    state: UserState,
                    userInfo: String
                  ) extends DAO

case class DoLogin(login:String,
                   pass: String) extends DAO



sealed trait UserState {
  var userState =""
}
final case class New_() extends UserState
{
  userState="New"
}
final case class Active() extends UserState
{
  userState="Active"
}
final case class Blocked() extends UserState
{
  userState="Blocked"
}
final case class Banned() extends UserState
{
  userState="Banned"
}

