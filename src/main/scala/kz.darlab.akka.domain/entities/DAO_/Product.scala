package kz.darlab.akka.domain.entities.DAO_

import kz.darlab.akka.domain.entities.{DAO}

case class Product(
                   id: Int,
                   category_id: Int,
                   name: String,
                   supplier: Option[String] = None,
                   description: Option[String]
                 ) extends DAO
case class Category(
                     id: Int,
                     name: String
                   )extends DAO


case class GetProductById (id :Int) extends DAO

case class GetProducts ()  extends  DAO

case class ProductList (l:List[Product])  extends  DAO